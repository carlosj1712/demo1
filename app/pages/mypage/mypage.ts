import { NavController } from 'ionic-angular';
import { Component } from '@angular/core';


/*
  Generated class for the MypagePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/mypage/mypage.html',
})
export class MypagePage {

  constructor(private navCtrl: NavController) {

  }

}
